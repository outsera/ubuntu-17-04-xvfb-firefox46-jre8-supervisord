FROM ubuntu:17.04
MAINTAINER TEXOIT "http://texoit.com/"

RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update
RUN apt-get install -y supervisor xvfb vim libgtk-3-0 libasound2
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get install -y oracle-java8-installer
RUN wget https://downloads.sourceforge.net/project/ubuntuzilla/mozilla/apt/pool/main/f/firefox-mozilla-build/firefox-mozilla-build_46.0.1-0ubuntu1_amd64.deb
RUN dpkg -i firefox-mozilla-build_46.0.1-0ubuntu1_amd64.deb
RUN rm firefox-mozilla-build_46.0.1-0ubuntu1_amd64.deb

ENV DISPLAY :99

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD init-xvfb.conf /etc/supervisor/conf.d/init-xvfb.conf

CMD ["/usr/bin/supervisord"]