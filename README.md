# README #

Base docker image from Ubuntu 17.04 with X Virtual Framebuffer, Firefox 46, Oracle JRE8u131 and supervisord.
Use this docker image if you need to run older selenium driver in a headless environment.

### How do I get set up? ###

Run with:
```sh
$ docker run --name <your name> -d texoit/ubuntu-17-04-xvfb-firefox46-jre8-supervisord
```

### Why extend this image? ###

If you ever need a scrapping service or something else that need a headless Firefox, you could just extend this image.
Just add a supervisord conf file for you program and that's it. No extra "CMD" or "ENTRYPOINT" needed!

Full example below:

**Dockerfile:**
```
FROM texoit/ubuntu-17-04-xvfb-firefox46-jre8-supervisord
MAINTAINER TEXOIT "http://texoit.com/"

RUN mkdir /my-program
ADD my-program.jar /my-program/my-program.jar
ADD init-my-program.conf /etc/supervisor/conf.d/init-my-program.conf
```

**init-my-program.conf Supervisord conf file:**
```
[program:my-program]
directory=/my-program/
command=java -jar my-program.jar -Xms512m -Xmx1024m -XX:+UseStringDeduplication
autostart=true
autorestart=true
redirect_stderr=true
```

**Build it:**
```sh
$ docker build --rm -t tag/my-program .
```

**Run with:**
```sh
$ docker run --name <your name> -d tag/my-program
```